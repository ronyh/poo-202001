package com.poo.aplicacion.controller;

import com.poo.aplicacion.dto.Libro;
import com.poo.aplicacion.service.SingletonLibroService;

import java.util.List;

public class Controller {
    public List<Libro> obtenerLibro(Libro libro){
        return SingletonLibroService.getInstance().buscarLibro(libro);
    }

    public static void main(String[] args) {
        Libro libro = new Libro();
        libro.setTitulo("Mecánica");
        Controller controller = new Controller();
        List<Libro> lista = controller.obtenerLibro(libro);
        for (Libro item: lista) {
            System.out.println(item);
            //System.out.println(item.getTitulo());
            //System.out.println(item.getAutor());
            //System.out.println(item.getCodigo());
        }
    }
}
