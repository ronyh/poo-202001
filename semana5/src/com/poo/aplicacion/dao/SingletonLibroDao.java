package com.poo.aplicacion.dao;

public class SingletonLibroDao {
    private static LibroDAO dao;
    public static LibroDAO getInstance(){
        if(dao == null){
            dao = new LibroDaoImpl();
        }
        return dao;
    }
}
