package com.poo.aplicacion.dao;

import com.poo.aplicacion.dto.Libro;

import java.util.ArrayList;
import java.util.List;

public class LibroDaoImpl implements LibroDAO{
    public List<Libro> buscarLibro(Libro libro) {
        List<Libro> lista = new ArrayList<Libro>();
        Libro libro1 = new Libro();
        libro1.setAutor("Tipler");
        libro1.setTitulo(libro.getTitulo());
        libro1.setCodigo(102379);
        lista.add(libro1);
        return lista;
    }
}
