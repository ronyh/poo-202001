package com.poo.aplicacion.dao;

import com.poo.aplicacion.dto.Libro;

import java.util.List;

public interface LibroDAO {
    public abstract List<Libro> buscarLibro(Libro libro);
}
