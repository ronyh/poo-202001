package com.poo.aplicacion.service;

import com.poo.aplicacion.dto.Libro;

import java.util.List;

public interface LibroService {
    public abstract List<Libro> buscarLibro(Libro libro);
}
