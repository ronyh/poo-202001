package com.poo.aplicacion.service;

import com.poo.aplicacion.dao.SingletonLibroDao;
import com.poo.aplicacion.dto.Libro;

import java.util.List;

public class LibroServiceImpl implements LibroService{
    public List<Libro> buscarLibro(Libro libro) {
        return SingletonLibroDao.getInstance().buscarLibro(libro);
    }
}
