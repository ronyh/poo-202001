package com.poo.aplicacion.service;

public class SingletonLibroService {
    private static LibroService service;
    public static LibroService getInstance(){
        if(service == null){
            service = new LibroServiceImpl();
        }
        return service;
    }
}
