package singleton;

public abstract class Singleton {
    private static Notificador notificador;
    public static Notificador getInstance(){
        if(notificador==null){
            notificador = new Notificador();
        }
        return notificador;
    }
}
