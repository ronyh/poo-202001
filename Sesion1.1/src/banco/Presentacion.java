package banco;

public class Presentacion {
    public static void main(String[] args) {
        Cuenta cuentaNueva = new Cuenta();
        cuentaNueva.setAgencia("Rimac");
        cuentaNueva.setCodigo("10000013");
        cuentaNueva.setNombre("Rony Hancco");
        cuentaNueva.setSaldo(500.00);

        Presentacion p = new Presentacion();
        p.imprimir(cuentaNueva);
    }
    public void imprimir(Cuenta cuenta){
        System.out.println("La cuenta es: "+cuenta.getCodigo());
        System.out.println("Que fue creada en la agencia: "+cuenta.getAgencia());
        System.out.println("Pertenece al cliente: "+ cuenta.getNombre());
        System.out.println("El saldo de la cuenta es:" +cuenta.getSaldo());
    }
}
