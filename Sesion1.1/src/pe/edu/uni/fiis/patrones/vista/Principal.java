package pe.edu.uni.fiis.patrones.vista;

import pe.edu.uni.fiis.patrones.Celular;
import pe.edu.uni.fiis.patrones.Reset;
import pe.edu.uni.fiis.patrones.Telefono;
import pe.edu.uni.fiis.patrones.Telefonoable;

public class Principal {
    public static void main(String[] args) {
        Reset reset = new Reset();
        Telefonoable telefono = new Celular();
        telefono.llamar();
        Telefono telefono1 = new Telefono();
        telefono1.setReset(reset);
        telefono1.getReset().inicializar();
    }
}
