package pe.edu.uni.fiis.atm.servicio;

public abstract class SingletonOperable {
    private static Operable operable;
    public static Operable getInstance(){
        if(operable==null){
            operable = new OperableImpl();
        }
        return operable;
    }
}
