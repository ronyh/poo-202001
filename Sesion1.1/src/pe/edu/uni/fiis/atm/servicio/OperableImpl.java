package pe.edu.uni.fiis.atm.servicio;

import pe.edu.uni.fiis.atm.dto.Cuenta;
import pe.edu.uni.fiis.atm.dto.MovimientoCuenta;

public class OperableImpl implements Operable{
    public void retirarEfectivo(Cuenta cuenta, Double monto) {
        if(cuenta != null && monto>0 && cuenta.getSaldo()>=monto){
            MovimientoCuenta movimientoCuenta = new MovimientoCuenta();
            movimientoCuenta.setCuentaOrigen(cuenta);
            movimientoCuenta.setMonto(monto);
            movimientoCuenta.setTipoOperacion("Retiro");
            cuenta.setSaldo(cuenta.getSaldo()-monto);
            /*
            MovimientoCuenta[] movimientos = cuenta.getLista();
            MovimientoCuenta[] nuevo = new MovimientoCuenta[movimientos.length+1];
            for (int i = 0; i < movimientos.length; i++) {
                nuevo[i] = movimientos[i];
            }
            nuevo[movimientos.length] = movimientoCuenta;
            cuenta.setLista(nuevo);
             */
            cuenta.getLista().add(movimientoCuenta);
        }


    }

    public void transferir(Cuenta cuentaOrigen, Cuenta cuentaDestino, Double monto) {
        if(cuentaOrigen != null && cuentaDestino != null && monto >0){
            cuentaOrigen.setSaldo(cuentaOrigen.getSaldo()-monto);
            cuentaDestino.setSaldo(cuentaDestino.getSaldo()+monto);

            MovimientoCuenta movimientoCuenta = new MovimientoCuenta();
            movimientoCuenta.setCuentaOrigen(cuentaOrigen);
            movimientoCuenta.setCuentaDestino(cuentaDestino);
            movimientoCuenta.setMonto(monto);
            movimientoCuenta.setTipoOperacion("Transferencia");

            cuentaOrigen.getLista().add(movimientoCuenta);
            cuentaDestino.getLista().add(movimientoCuenta);
        }
    }

    public void revisarSaldo(Cuenta cuenta) {
        if(cuenta != null){
            System.out.println(cuenta.toString());
        }
    }
}
