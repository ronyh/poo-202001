package com.qaraywa.servicio.impl;


public class SingletonServicio {
    private static LibroDiarioServicioImpl servicio = null;
    public static LibroDiarioServicioImpl getInstance(){
        if(servicio == null){
            servicio = new LibroDiarioServicioImpl();
        }
        return servicio;
    }

    public class Libro{
        public void imprimir(int a, int b, int c){

        }
        public void imprimir(int... a){

        }
    }
}
